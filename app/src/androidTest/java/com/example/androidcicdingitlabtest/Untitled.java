package com.example.androidcicdingitlabtest;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.By;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.logging.Level;

public class Untitled {
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Untitled";
    protected AndroidDriver driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();

    @Before
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "cc7ef3c2");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.example.androidcicdingitlabtest");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".MainActivity");
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }

    @Test
    public void testUntitled() {
        driver.findElement(By.xpath("//*[@id='et']")).click();
        driver.findElement(By.xpath("//*[@id='et']")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_del']]")).click();
        driver.findElement(By.xpath("//*[@class='android.widget.TextView' and ./parent::*[@id='key_pos_1_2']]")).click();
        driver.findElement(By.xpath("(//*[@id='key_pos_0_7']/*[@class='android.widget.TextView'])[1]")).click();
        driver.findElement(By.xpath("//*[@class='android.widget.TextView' and ./parent::*[@id='key_pos_1_8']]")).click();
        driver.findElement(By.xpath("//*[@class='android.widget.TextView' and ./parent::*[@id='key_pos_1_1']]")).click();
        driver.findElement(By.xpath("//*[@class='android.widget.TextView' and ./parent::*[@id='key_pos_1_5']]")).click();
        driver.findElement(By.xpath("(//*[@id='key_pos_0_8']/*[@class='android.widget.TextView'])[1]")).click();
        driver.findElement(By.xpath("//*[@class='android.widget.TextView' and ./parent::*[@id='key_pos_1_2']]")).click();
        driver.findElement(By.xpath("//*[@class='android.widget.TextView' and ./parent::*[@id='key_pos_2_5']]")).click();
        driver.findElement(By.xpath("(//*[@id='key_pos_0_2']/*[@class='android.widget.TextView'])[1]")).click();
        driver.findElement(By.xpath("//*[@class='android.widget.TextView' and ./parent::*[@id='key_pos_1_7']]")).click();
        driver.findElement(By.xpath("//*[@id='et']")).click();
        driver.findElement(By.xpath("//*[@id='key_pos_del']")).click();
        driver.findElement(By.xpath("//*[@id='0_resource_name_obfuscated' and @class='android.widget.ImageView' and ./parent::*[@id='key_pos_shift']]")).click();
        driver.findElement(By.xpath("//*[@class='android.widget.TextView' and ./parent::*[@id='key_pos_1_2']]")).click();
        driver.findElement(By.xpath("//*[@class='android.widget.FrameLayout' and ./*[@class='android.widget.LinearLayout' and ./*[@id='0_resource_name_obfuscated' and @class='android.widget.FrameLayout']]]")).click();
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}