package com.example.androidcicdingitlabtest;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class Untitled {
    private String reportDirectory = "reports";
    private String reportFormat = "xml";
    private String testName = "Untitled";
    protected AndroidDriver driver = null;

    DesiredCapabilities dc = new DesiredCapabilities();

    @Before
    public void setUp() throws MalformedURLException {
        dc.setCapability("reportDirectory", reportDirectory);
        dc.setCapability("reportFormat", reportFormat);
        dc.setCapability("testName", testName);
        dc.setCapability(MobileCapabilityType.UDID, "cc7ef3c2");
        dc.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.example.androidcicdingitlabtest");
        dc.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".MainActivity");
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), dc);
        driver.setLogLevel(Level.INFO);
    }
    @Test
    public void testUntitled() {
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.executeScript("seetest:client.deviceAction(\"BKSP\")");
        driver.getKeyboard().sendKeys("Dilshodbek");
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}